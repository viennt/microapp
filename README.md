# Microapp

## Description

The Micro Frontend project is a system composed of multiple independent small front-end applications (Micro Frontends) that can be developed, deployed, and managed independently. The goal of the project is to create a flexible, maintainable, and scalable architecture.

## Micro Frontends

1. **main-app:** The Main App acts as the cornerstone within our Micro Frontend architecture, functioning as a dedicated space for essential UI components and shared libraries. It serves as a unified repository for fundamental user interface elements, fostering consistency and collaboration across the entire system.

2. **app1:** Demo app has it own routes and languages.

3. **app2:** Demo app has it own routes and languages.

...

## Installation Guide

1. Clone the repository to your local machine:

    ```bash
    git clone https://gitlab.com/viennt/microapp.git
    ```

2. Navigate to the project directory:

    ```bash
    cd microapp
    ```

3. Install dependencies: (You need to install pnpm https://pnpm.io/)

    ```bash
    pnpm run setup
    ```

4. Run all apps at the same time

    ```bash
    pnpm run dev
    ```
