export default {
	apiServer: "https://example.com",
	remoteApp: [
		{
			name: "app1",
			remoteUrl: "http://localhost:4001/assets/remoteEntry.js"
		},
		{
			name: "app2",
			remoteUrl: "http://localhost:4002/assets/remoteEntry.js"
		}
	]
}