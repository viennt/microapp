import { createApp, defineAsyncComponent } from "vue";
import App from "./App.vue";
import PrimeVue from "primevue/config";
import router from "./helper/router";
import stores from "./helper/stores";
import i18n from "./helper/i18n"
import api from "./helper/api";
import { createPinia } from "pinia";
import {
  __federation_method_setRemote,
  __federation_method_getRemote, 
  __federation_method_unwrapDefault
} from 'virtual:__federation__';
import config from "./config";
import { registerLayouts } from "./helper/layout";

import InputText from "primevue/inputtext";
import InputNumber from "primevue/inputnumber";
import Button from "primevue/button";
import TabView from 'primevue/tabview';
import TabPanel from 'primevue/tabpanel';
import Tag from 'primevue/tag';
import SelectButton from 'primevue/selectbutton';
import Textarea from 'primevue/textarea';
import Dropdown from 'primevue/dropdown';
import Chips from 'primevue/chips';
import AutoComplete from 'primevue/autocomplete';
import Toast from 'primevue/toast';
import ToastService from 'primevue/toastservice';
import DialogService from 'primevue/dialogservice';
import ConfirmationService from 'primevue/confirmationservice';
import Calendar from 'primevue/calendar';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup';   // optional
import Row from 'primevue/row';
import TreeTable from 'primevue/treetable';

import "@/style.css";
import "primevue/resources/themes/lara-light-indigo/theme.css";
import "primeflex/primeflex.scss";
import "primeicons/primeicons.css";

window.app = createApp(App)
    .use(router)
    .use(createPinia())
    .use(stores)
    .use(PrimeVue)
    .use(ToastService)
    .use(DialogService)
    .use(ConfirmationService)
    .use(i18n)
    .use(api)
    .component("InputText", InputText)
    .component("InputNumber", InputNumber)
    .component("Button", Button)
    .component("TabView", TabView)
    .component("TabPanel", TabPanel)
    .component("Tag", Tag)
    .component("SelectButton", SelectButton)
    .component("Textarea", Textarea)
    .component("Dropdown", Dropdown)
    .component("Chips", Chips)
    .component("AutoComplete", AutoComplete)
    .component("Toast", Toast)
    .component("Calendar", Calendar)
    .component("DataTable", DataTable)
    .component("Column", Column)
    .component("ColumnGroup", ColumnGroup)
    .component("Row", Row)
    .component("TreeTable", TreeTable);

registerLayouts(window.app);

config.remoteApp.forEach(microapp => {
  __federation_method_setRemote(microapp.name, { 
    url:() => Promise.resolve(microapp.remoteUrl), 
    format: 'esm', from: 'vite' 
  });
  // window.app.component(`${microapp.name}-app`, defineAsyncComponent(() => {
  //   return __federation_method_getRemote(microapp.name, `./app`).then(moduleWraped => {
  //     return __federation_method_unwrapDefault(moduleWraped);
  //   })
  // }))
})
Promise.all(config.remoteApp.map(a => __federation_method_getRemote(a.name, `./setup`)))
.then(_ => {
  window.app.mount("#app");
  if ( window.$router.currentRoute.value.fullPath!='/' )
    window.$router.replace(window.$router.currentRoute.value.fullPath);
})

