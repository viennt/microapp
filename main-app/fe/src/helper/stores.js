import { userStore } from "../stores/user";

const stores = {};
const setupStores = () => {
  if (window.stores) {
    window.location.reload();
  }
  Object.assign(stores, {
    user: userStore()
  });
  window.stores = stores;
};

// Promise.resolve().then(setupStores);


export default {
  install: (app, options) => {
    setTimeout(setupStores)
    app.config.globalProperties.$stores = stores;
  }
};
