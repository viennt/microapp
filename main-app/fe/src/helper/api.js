import config from "../config";

class ApiServer {
  async call(cmd, args = {}, method = "") {
    if (!method) method = Object.keys(args).length === 0 ? "GET" : "POST";
    let url = config.apiServer + "/" + cmd;
    if (Object.keys(args).length && method === "GET") { url += "?" + new URLSearchParams(args).toString(); }
    const token = await localStorage.getItem("token");
    const headers = {};
    if (token) headers.token = token;
    const options = method === "GET"
      ? { headers }
      : {
          method,
          headers: {
            ...headers,
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify(args)
        };
    return fetch(url, options).then(res => res.json()).then(res => {
      if ( res.error ) {
        window.$toast.add({
          severity: 'info',
          summary: 'Có lỗi xảy ra',
          detail: res.error.message,
          life: 3000
        });
      }
      return res;
    }).catch(e => {
      window.$toast.add({
        severity: 'info',
        summary: 'Có lỗi xảy ra',
        detail: e.message,
        life: 3000
      });
      throw e;
    });
  }

  async currentUser(user) {
    return this.call("user/info", user).then(res => {
      if ( res.token ) localStorage.setItem("token", res.token);
      return res;
    }).catch(e => {
      localStorage.removeItem("token")
    });
  }
}

export default {
  install: (app, options) => {
    window.$api = new ApiServer();
    app.config.globalProperties.$api = window.$api;
  }
};
