import { createRouter, createWebHistory } from "vue-router";
import Home from "../pages/Home/index.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      component: Home,
      meta: { layout: "layout1" }
    },
  ],
});

window.$router = router;
export default router;