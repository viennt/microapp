import { App } from 'vue';

/**
 * Register layouts in the app instance
 *
 * @param {App<Element>} app
 */
export function registerLayouts(app) {
  const layouts = import.meta.globEager('/src/pages/Layout/*.vue');

  Object.entries(layouts).forEach(([file, layout]) => {
    const name = file.split("/").pop().replace(".vue", "");
    app.component(name, layout?.default);
  });
}