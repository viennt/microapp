import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from 'path';
import topLevelAwait from "vite-plugin-top-level-await";
import federation from "@originjs/vite-plugin-federation";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    topLevelAwait({
      // The export name of top-level await promise for each chunk module
      promiseExportName: "__tla",
      // The function to generate import names of top-level await promise in each chunk module
      promiseImportName: i => `__tla_${i}`
    }),
    vue(),
    federation({
      name: 'host-app',
      filename: 'remoteEntry.js',
      exposes: {
        './app': "./src/App.vue",
        './layout1': "./src/pages/Layout/layout1.vue",
        './layout2': "./src/pages/Layout/layout2.vue"
      },
      remotes: {
        remote_app: "",
      },
      shared: {
        'vue': { version: "3.3.4" }, 
        'primevue/button': { version: "0.0.0" },
        'primevue/inputtext': { version: "0.0.0" },
        'primevue/inputnumber': { version: "0.0.0" },
        'primevue/tabview': { version: "0.0.0" },
        'primevue/tabpanel': { version: "0.0.0" },
        'primevue/tag': { version: "0.0.0" },
        'primevue/selectbutton': { version: "0.0.0" },
        'primevue/textarea': { version: "0.0.0" },
        'primevue/dropdown': { version: "0.0.0" },
        'primevue/chips': { version: "0.0.0" },
        'primevue/autocomplete': { version: "0.0.0" },
        'primevue/toast': { version: "0.0.0" },
        'primevue/toastservice': { version: "0.0.0" },
        'primevue/dialogservice': { version: "0.0.0" },
        'primevue/confirmationservice': { version: "0.0.0" },
        'primevue/calendar': { version: "0.0.0" },
        'primevue/datatable': { version: "0.0.0" },
        'primevue/column': { version: "0.0.0" },
        'primevue/columngroup': { version: "0.0.0" },
        'primevue/row': { version: "0.0.0" },
        'primevue/treetable': { version: "0.0.0" },
        'pinia': { version: "2.1.7" }
      }
    })
  ],
  build: {
    outDir: 'dist',
    rollupOptions: {
      output: {
        entryFileNames: `assets/[name].module.js`,
        chunkFileNames: `assets/[name].module.js`,
        assetFileNames: `assets/[name].[ext]`
      }
    },
    // target: ["chrome89", "edge89", "firefox89", "safari13"]
  },
  resolve: {
    alias: [{ find: '@', replacement: resolve(__dirname, 'src') }],
  },
  server: {
    port: 4000
  },
  preview: {
    port: 4000
  },
});
