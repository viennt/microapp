import { createApp, defineAsyncComponent } from "vue";
import App from "./App.vue";
import PrimeVue from "primevue/config";

window.app = createApp(App);

import InputText from "primevue/inputtext";
import InputNumber from "primevue/inputnumber";
import Button from "primevue/button";
import TabView from 'primevue/tabview';
import TabPanel from 'primevue/tabpanel';
import Tag from 'primevue/tag';
import SelectButton from 'primevue/selectbutton';
import Textarea from 'primevue/textarea';
import Dropdown from 'primevue/dropdown';
import Chips from 'primevue/chips';
import AutoComplete from 'primevue/autocomplete';
import Toast from 'primevue/toast';
import ToastService from 'primevue/toastservice';
import DialogService from 'primevue/dialogservice';
import ConfirmationService from 'primevue/confirmationservice';
import Calendar from 'primevue/calendar';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup';   // optional
import Row from 'primevue/row';
import TreeTable from 'primevue/treetable';

// import "@/style.css";
import "primevue/resources/themes/lara-light-indigo/theme.css";
import "primeflex/primeflex.scss";
import "primeicons/primeicons.css";


window.app
    .use(PrimeVue)
    .component("InputText", InputText)
    .component("InputNumber", InputNumber)
    .component("Button", Button)
    .component("TabView", TabView)
    .component("TabPanel", TabPanel)
    .component("Tag", Tag)
    .component("SelectButton", SelectButton)
    .component("Textarea", Textarea)
    .component("Dropdown", Dropdown)
    .component("Chips", Chips)
    .component("AutoComplete", AutoComplete)
    .component("Toast", Toast)
    .component("Calendar", Calendar)
    .component("DataTable", DataTable)
    .component("Column", Column)
    .component("ColumnGroup", ColumnGroup)
    .component("Row", Row)
    .component("TreeTable", TreeTable);
window.app.mount("#app");
