// Install languages
import en from '/src/languages/en';
import vi from '/src/languages/vi';

if ( window.i18n ) {
	window.i18n.setLocaleMessage('vi', vi);
	window.i18n.setLocaleMessage('en', en);
}

// Install routes
if ( window.$router ) {
	const addedRoute = window.$router.getRoutes();
	const routes = [
	{
      path: "/app2",
      component: () => import("/src/App.vue"),
      meta: {
      	layout: "layout1"
      }
    }
  ];
	routes.forEach(r => {
		const existed = addedRoute.find(r2 => r2.path==r.path);
		if ( !existed ) {
		  window.$router.addRoute(r)
		}
	});
}

if ( window.app ) {

}